package zadanie2;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ReadConfig rc = new ReadConfig();
        rc.readConfig();
        Game g = new Game();
        for (int i = 1; i < MySettings.getINSTANCE().getIlosc_rund()+1; i++) {
            System.out.println("Runda nr " + i);
            g.play();
            g.newRound();
        }
        System.out.println("Koniec gry, po "+MySettings.getINSTANCE().getIlosc_rund() + " zdobyłeś "+ g.getPunkty() + " punktów");


    }
}
