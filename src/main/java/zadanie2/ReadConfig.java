package zadanie2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class ReadConfig {

    public void readConfig() throws FileNotFoundException {
        File config = new File("C:\\Users\\Tomek\\Projekty\\Singleton\\src\\main\\java\\zadanie2\\Config");
        Scanner sc = new Scanner(config);
        while (sc.hasNextLine()) {
            String[] inputArgs = sc.nextLine().split("=");
            if (inputArgs[0].contains("zakres_liczby_1")) {
                MySettings.getINSTANCE().setZakres_liczb_1(Integer.parseInt(inputArgs[1]));
            } else if (inputArgs[0].contains("zakres_liczby_2")) {
                MySettings.getINSTANCE().setZakres_liczb_2(Integer.parseInt(inputArgs[1]));
            } else if (inputArgs[0].contains("ilosc_rund")) {
                MySettings.getINSTANCE().setIlosc_rund(Integer.parseInt(inputArgs[1]));
            } else if (inputArgs[0].contains("dostepne_dzialania")) {
                MySettings.getINSTANCE().setDostepne_dzialania(inputArgs[1]);
            }
            if (inputArgs[1].contains("+")) {
                MySettings.getINSTANCE().getUsedOperators().add(Enums.ADD);
            }
            if (inputArgs[1].contains("-")) {
                MySettings.getINSTANCE().getUsedOperators().add(Enums.SUB);
            }
            if (inputArgs[1].contains("*")) {
                MySettings.getINSTANCE().getUsedOperators().add(Enums.MUL);
            }
            if (inputArgs[1].contains("/")) {
                MySettings.getINSTANCE().getUsedOperators().add(Enums.DIV);
            }
        }
    }


}

