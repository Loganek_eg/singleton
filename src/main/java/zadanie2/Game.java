package zadanie2;

import lombok.Getter;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

@Getter
public class Game {
    private int number1;
    private int number2;
    int wynik_uzytkownika;
    private int punkty = 0;
    private Random random = new Random();
    private Enums randomEnum = getRandomEnum();

    public Game() {
    }

    private void randomNumbers() {
        this.number1 = random.nextInt(MySettings.getINSTANCE().getZakres_liczb_1());
        this.number2 = random.nextInt(MySettings.getINSTANCE().getZakres_liczb_2()) + 1;
    }

    private Enums getRandomEnum() {
        int randomindex = random.nextInt(MySettings.getINSTANCE().getUsedOperators().size());
        return MySettings.getINSTANCE().getUsedOperators().get(randomindex);
    }

    private int getResult() {
        Enums o = randomEnum;
        switch (o) {
            case ADD: {
                return number1 + number2;
            }
            case SUB: {
                return number1 - number2;
            }
            case DIV: {
                return number1 / number2;
            }
            case MUL: {
                return number1 * number2;
            }
        }
        return 0;
    }

    public void newRound() {
        randomNumbers();
        getRandomEnum();
        getResult();
    }

    public void play() {
        randomNumbers();
        switch (randomEnum) {
            case ADD:
                System.out.println("Podaj wynik : " + number1 + " + " + number2);
                break;
            case SUB:
                System.out.println("Podaj wynik : " + number1 + " - " + number2);
                break;
            case MUL:
                System.out.println("Podaj wynik : " + number1 + " * " + number2);
                break;
            case DIV:
                System.out.println("Podaj wynik : " + number1 + " / " + number2);
                break;
        }
        Scanner sc = new Scanner(System.in);
        try {
            wynik_uzytkownika = sc.nextInt();
        } catch (InputMismatchException ime) {
            System.out.println("Wpisałeś błędną wartość");
        }
        if (wynik_uzytkownika == getResult()) {
            punkty++;
            System.out.println("Zdobyłeś 1 punkt");
        } else {
            System.out.println("Błędny wynik, nie zdobyłeś punktów");

        }
    }

}



