package zadanie2;


import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
public class MySettings {
    private List<Enums> usedOperators = new LinkedList<>();
    private int zakres_liczb_1;
    private int zakres_liczb_2;
    private int ilosc_rund;
    private String dostepne_dzialania;

    private static MySettings INSTANCE;

    public static MySettings getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new MySettings();
        }
        return INSTANCE;
    }

    public int getZakres_liczb_1() {
        return zakres_liczb_1;
    }

    public void setZakres_liczb_1(int zakres_liczb_1) {
        this.zakres_liczb_1 = zakres_liczb_1;
    }

    public int getZakres_liczb_2() {
        return zakres_liczb_2;
    }

    public void setZakres_liczb_2(int zakres_liczb_2) {
        this.zakres_liczb_2 = zakres_liczb_2;
    }

    public int getIlosc_rund() {
        return ilosc_rund;
    }

    public void setIlosc_rund(int ilosc_rund) {
        this.ilosc_rund = ilosc_rund;
    }

    public String getDostepne_dzialania() {
        return dostepne_dzialania;
    }

    public void setDostepne_dzialania(String dostepne_dzialania) {
        this.dostepne_dzialania = dostepne_dzialania;
    }


    public List<Enums> getUsedOperators() {
        return usedOperators;
    }

    @Override
    public String toString() {
        return "MySettings{" +
                "zakres_liczb_1=" + zakres_liczb_1 +
                ", zakres_liczb_2=" + zakres_liczb_2 +
                ", ilosc_rund=" + ilosc_rund +
                ", dostepne_dzialania='" + dostepne_dzialania + '\'' +
                '}';
    }
}
