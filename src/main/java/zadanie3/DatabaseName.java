package zadanie3;

public enum DatabaseName {
    DB_USERS("C:\\Users\\Logan\\Projekty\\Singleton\\src\\main\\java\\zadanie3\\users"),
    DB_ORDER("C:\\Users\\Logan\\Projekty\\Singleton\\src\\main\\java\\zadanie3\\orders"),
    DB_REQUESTS("C:\\Users\\Logan\\Projekty\\Singleton\\src\\main\\java\\zadanie3\\requests.txt");

    private String dbName;

    DatabaseName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbName() {
        return dbName;
    }
}
