package zadanie1;

import java.util.LinkedList;
import java.util.List;

public class TicketGenerator {
    private int counter = 0;
    private final static TicketGenerator tg = new TicketGenerator();
    private List<Integer> ticketList = new LinkedList<>();

    private TicketGenerator() {
    }

    public static TicketGenerator getTg() {
        return tg;
    }

    public void call() {
    }

    public List<Integer> getCalls() {
        return ticketList;
    }


    public int generateTicket() {
        return ++counter;
    }

}
