package zadanie1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while(true) {
            Scanner sc = new Scanner(System.in);
            HealthDepartment hd = new HealthDepartment();

            System.out.println(TicketGenerator.getTg().generateTicket());
            CashRegistry cs = new CashRegistry();

            WaitingRoom wr = new WaitingRoom();
            System.out.println(cs.generateTicket());
            System.out.println(hd.generateTicket());
            System.out.println(wr.generateTicket());
        }
    }
}
